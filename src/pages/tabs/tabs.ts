import { Component } from '@angular/core';

import { FavoritePage } from '../favorite/favorite';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FavoritePage;

  constructor() {

  }
}
