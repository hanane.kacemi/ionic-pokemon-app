import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListPokemonProvider } from '../../providers/list-pokemon/list-pokemon';
import { StatisticsPage } from '../statistics/statistics';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pokemons : any;
  constructor(public navCtrl: NavController,public listPokemon : ListPokemonProvider) {
   
    
  }

  ngOnInit(){
    this.listPokemon.getData('https://pokeapi.co/api/v2/pokemon').subscribe( data => this.pokemons = data )
  }

  detailPokemon(id :number){
    this.navCtrl.push(StatisticsPage, {'id' : id});
  }

  addToFavorite(id){
    this.listPokemon.addFavorite(id);
  }
  

}
