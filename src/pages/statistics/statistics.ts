import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ListPokemonProvider } from '../../providers/list-pokemon/list-pokemon';

@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html'
})
export class StatisticsPage {

  pokemon : any;

  constructor(public navCtrl: NavController, public param: NavParams,public listPokemon : ListPokemonProvider) {

  }

  ngOnInit(){
    this.listPokemon.getData('https://pokeapi.co/api/v2/pokemon/' + this.param.get('id'))
    .subscribe( data =>  this.pokemon = data );
  }

}
