import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListPokemonProvider } from '../../providers/list-pokemon/list-pokemon';
import { StatisticsPage } from '../statistics/statistics';

@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html'
})
export class FavoritePage {

  myFavorites : any;
  constructor(public navCtrl: NavController,public listPokemon : ListPokemonProvider) {

  }

  ionViewWillEnter(){
    this.myFavorites = this.listPokemon.getFavorites();
  }

  detailPokemon(id :number){
    this.navCtrl.push(StatisticsPage, {'id' : id});
  }

}
