import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ListPokemonProvider {
  
  favorites : Array<number> = [];

  constructor(public http: HttpClient) {
    
  }

  getData(url){
    return this.http.get(url);
  }

  addFavorite(id){
  this.favorites.push(id);
  }

  getFavorites(){
    return this.favorites;
  }

}
